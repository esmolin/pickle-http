<?php
/**
 * Замена класса pecl_http 1.7 на чистом PHP
 */ 
class HTTPRequest
{
    const METH_POST=1;
    const METH_GET=3;
    
    const AUTH_BASIC=1;
    const SSL_VERSION_ANY=0;

    static $methods = [
        HTTPRequest::METH_GET => 'GET',
        HTTPRequest::METH_POST => 'POST',
    ];

    static protected $_sslOptionsPeclToContextMap = [
        'cert' => 'local_cert',
        'key' => 'local_pk',
        'certpasswd' => 'passphrase',
        'keypasswd' => 'passphrase',
        'verifypeer' => 'verify_peer',
        'verifyhost' => 'verify_peer_name',
        'cainfo' => 'cafile'
    ];

    protected $_url;
    protected $_requestMethod;
    protected $_options = ['timeout' => 60, 'ssl' => []];
    protected $_queryData;
    protected $_postFields = [];
    protected $_postFiles = [];
    protected $_headers = [];
    protected $_body = '';
    protected $_boundary = '';

    protected $_response;

    public function __construct($url='', $request_method=HTTPRequest::METH_GET, $options=[])
    {
        $this->_url = $url;
        $this->_requestMethod = $request_method;
        $this->setOptions($options);
    }

    public function addHeaders ($headers)
    {
        $this->_headers = array_merge($this->_headers, $headers);
    }

    public function addPostFile($name, $file, $content_type = "application/x-octetstream")
    {
        $this->_postFiles[] = compact('name', 'file', 'content_type');
    }

    public function send()
    {
        $parts = parse_url($this->_url);
        $parts['query'] = $this->_queryData;
        $url = self::unparseUrl($parts);
        $this->_processPostFields();
        $this->_processPostFiles();
        $this->_processAuthorization();
        $stream_opts = ['http' => [
            'method' => self::$methods[$this->_requestMethod],
            'timeout' => $this->_options['timeout'],
            'ignore_errors' => true,
            'header' => self::headersToString($this->_headers)
            ]];

        if ($this->_body) {
            $stream_opts['http']['content'] = $this->_body;
        }

        if (is_array($this->_options['ssl'])) {
            // only HTTPRequest::SSL_VERSION_ANY version is supported, another will throw a notice during mapping
            if (array_key_exists('version', $this->_options['ssl']) && $this->_options['ssl']['version'] == self::SSL_VERSION_ANY) {
                unset($this->_options['ssl']['version']);
            }
            foreach ($this->_options['ssl'] as $k=>$v) {
                if (isset(self::$_sslOptionsPeclToContextMap[$k])) {
                    $stream_opts['ssl'][self::$_sslOptionsPeclToContextMap[$k]] = $v;
                } else {
                    trigger_error(sprintf('Unsupported ssl option: %s', $k));
                }
            }
        }
        $ctx = stream_context_create($stream_opts);
        $response_body = file_get_contents($url, false, $ctx);
        $this->_response = new HTTPMessage();
        $headers = $http_response_header;
        $status_line = array_shift($headers);
        preg_match("/^HTTP\/(?<http_version>\d\.\d)\s(?<code>\d+)\s(?<status>.+)$/i", $status_line, $status_parts);
        $this->_response->setBody($response_body);

        $assoc_headers = [];
        foreach ($headers as $line) {
            if (preg_match("/^(?<key>[A-z0-9\-]+):\s?(?<value>.*)$/i", $line, $parts)) {
                $assoc_headers[$parts['key']] = $parts['value'];
            }
        }

        $this->_response->setHeaders($assoc_headers);
        $this->_response->setResponseCode($status_parts['code']);
        $this->_response->setResponseStatus($status_parts['status']);
        $this->_response->setHttpVersion($status_parts['http_version']);
        return $this->_response;
    }

    public function getBody()
    {
        $this->_processPostFields();
        $this->_processPostFiles();
        return $this->_body;
    }

    public function getHeaders()
    {
        return $this->_headers;
    }

    public function getRawPostData()
    {
        return $this->_body;
    }

    public function getMethod()
    {
        return $this->_requestMethod;
    }

    public function getQueryData()
    {
        return $this->_queryData;
    }

    public function getOptions()
    {
        return $this->_options;
    }

    public function getPostFields ()
    {
        return $this->_postFields;
    }

    public function getRawRequestMessage()
    {
        $urlparts = parse_url(sprintf('%s?%s', $this->_url, $this->_queryData));
        $head = sprintf('%s %s%s HTTP/1.1',
            http_request_method_name($this->_requestMethod),
            $urlparts['path'], (isset($urlparts['query']) ? '?'.$urlparts['query'] : ''));
        return sprintf("%s\r\n%s\r\n\r\n%s", $head, self::headersToString($this->_headers), $this->_body);
    }

    public function getRawResponseMessage()
    {
        return $this->_response ? $this->_response->toString() : '';
    }

    public function getResponseBody()
    {
        return $this->_response ? $this->_response->getBody() : false;
    }

    public function getResponseCode()
    {
        return $this->_response ? $this->_response->getResponseCode() : 0;
    }

    public function getResponseHeader($name=null)
    {
        if ($this->_response) {
            if ($name) {
                $result = $this->_response->getHeader($name);
            } else {
                $result = $this->_response->getHeaders();
            }
        } else {
            $result = false;
        }
        return $result;
    }

    public function getResponseStatus()
    {
        return $this->_response ? $this->_response->getResponseStatus() : '';
    }
    
    public function getSslOptions ()
    {
        return $this->_options['ssl'];
    }
    
    public function getUrl()
    {
        return $this->_url;
    }

    public function setBody($body)
    {
        $this->_body = $body;
    }

    public function setHeaders($headers)
    {
        $this->_headers = $headers;
    }

    public function setMethod($method)
    {
        $this->_requestMethod = $method;
    }

    public function setOptions ($options)
    {
        $this->_options = array_merge($this->_options, $options);
    }

    public function setQueryData ($query_data = null)
    {
        if (is_array($query_data)) {
            $this->_queryData = http_build_query($query_data);
        } else {
            $this->_queryData = $query_data;
        }
    }

    public function setPostFields ($post_data)
    {
        if (is_array($post_data)) {
            $this->_postFields = $post_data;
        } elseif (! $post_data) {
            $this->_postFields = [];
        } else {
            return false;
        }
        return true;
    }
    
    public function setSslOptions($options)
    {
        if (is_array($options)) {
            $this->_options['ssl'] = $options;
        } elseif (!$options) {
            $this->_options['ssl'] = [];
        } else {
            $this->_options['ssl'] = [];
            trigger_error('$options must be an array or empty value');
        }

    }
    
    public function setUrl($url)
    {
        $this->_url = $url;
    }

    static public function unparseUrl($parsed_url)
    {
        $scheme   = isset($parsed_url['scheme']) ? $parsed_url['scheme'] . '://' : '';
        $host     = isset($parsed_url['host']) ? $parsed_url['host'] : '';
        $port     = isset($parsed_url['port']) ? ':' . $parsed_url['port'] : '';
        $user     = isset($parsed_url['user']) ? $parsed_url['user'] : '';
        $pass     = isset($parsed_url['pass']) ? ':' . $parsed_url['pass']  : '';
        $pass     = ($user || $pass) ? "$pass@" : '';
        $path     = isset($parsed_url['path']) ? $parsed_url['path'] : '';
        $query    = isset($parsed_url['query']) ? '?' . $parsed_url['query'] : '';
        $fragment = isset($parsed_url['fragment']) ? '#' . $parsed_url['fragment'] : '';
        return "$scheme$user$pass$host$port$path$query$fragment";
    }

    static public function headersToString($headers)
    {
        $result = [];
        foreach ($headers as $key=>$value) {
            $result[] = sprintf("%s: %s", $key, $value);
        }
        return implode("\r\n", $result);
    }
    protected function _processPostFiles()
    {
        $body = [];
        if ($this->_postFiles) {
            $this->_boundary = substr(md5(rand(0, 32000)), 0, 20);
            foreach ($this->_postFiles as $f) {
                $body[] = '--' . $this->_boundary;
                $body[] = sprintf('Content-Disposition: form-data; name="%s"; filename="%s"',
                    $f['name'], basename($f['file']));
                $body[] = sprintf('Content-Type: %s', $f['content_type']);
                $body[] = 'Content-Transfer-Encoding: binary';
                $body[] = '';
                $body[] = file_get_contents($f['file']);

            }
            $body[] = '--' . $this->_boundary;
            $body[] = '';
            $this->_headers['Content-Type'] = sprintf('multipart/form-data; boundary=%s', $this->_boundary);

            // re-add post fields
            if ($this->_postFields) {
                foreach ($this->_postFiles as $f=>$v) {
                    $body[] = '--' . $this->_boundary;
                    $body[] = sprintf('Content-Disposition: form-data; name="%s"', $f);
                    $body[] = '';
                    $body[] = $v;
                }
            }
            $this->_body = implode("\r\n", $body);
        }
    }

    protected function _processPostFields()
    {
        if ($this->_postFields) {
            $this->_body = http_build_query($this->_postFields);
            $this->_headers['Content-Type'] = 'application/x-www-form-urlencoded';
        }
    }
    
    protected function _processAuthorization()
    {
        if (array_key_exists('httpauth', $this->_options)) {
            if (!array_key_exists('httpauthtype', $this->_options) || $this->_options['httpauthtype'] == self::AUTH_BASIC) {
                $this->addHeaders(['Authorization' => sprintf(
                    'Basic %s', base64_encode($this->_options['httpauth']))]);
            } else {
                trigger_error("Unsupported authorization type in 'httpauthtype'");
            }
        }
    }
}

class HTTPMessage
{
    protected $_body;
    protected $_headers;
    protected $_httpVersion;
    protected $_responseCode;
    protected $_responseStatus;

    public function getBody()
    {
        return $this->_body;
    }

    public function getHeader($name)
    {
        return isset($this->_headers[$name]) ? $this->_headers[$name] : false;
    }

    public function getHeaders()
    {
        return $this->_headers;
    }

    public function getHttpVersion()
    {
        return $this->_httpVersion;
    }

    public function getResponseCode()
    {
        return $this->_responseCode;
    }

    public function getResponseStatus()
    {
        return $this->_responseStatus;
    }

    public function setBody($body)
    {
        $this->_body = $body;
    }

    public function setHeaders($headers)
    {
        $this->_headers = $headers;
    }

    public function setHttpVersion($http_version)
    {
        $this->_httpVersion = $http_version;
    }

    public function setResponseCode($response_code)
    {
        $this->_responseCode = $response_code;
    }

    public function setResponseStatus($response_status)
    {
        $this->_responseStatus = $response_status;
    }

    public function toString($include_parent=false)
    {
        return sprintf("HTTP/%s %s %s\r\n%s\r\n\r\n%s",
            $this->_httpVersion,
            $this->_responseCode,
            $this->_responseStatus,
            HttpRequest::headersToString($this->_headers),
            $this->_body);
    }
}

//class HttpRequest {
//    public bool addCookies ( array $cookies );
//    public bool addHeaders ( array $headers );
//    public bool addPostFields ( array $post_data );
//    public bool addPostFile ( string $name , string $file [, string $content_type = "application/x-octetstream" ] );
//    public bool addPutData ( string $put_data );
//    public bool addQueryData ( array $query_params );
//    public bool addRawPostData ( string $raw_post_data );
//    public bool addSslOptions ( array $options );
//    public void clearHistory ( void );
//    public __construct ([ string $url [, int $request_method = HTTP_METH_GET [, array $options ]]] );
//    public bool enableCookies ( void );
//    public string getContentType ( void );
//    public array getCookies ( void );
//    public array getHeaders ( void );
//    public HttpMessage getHistory ( void );
//    public int getMethod ( void );
//    public array getOptions ( void );
//    public array getPostFields ( void );
//    public array getPostFiles ( void );
//    public string getPutData ( void );
//    public string getPutFile ( void );
//    public string getQueryData ( void );
//    public string getRawPostData ( void );
//    public string getRawRequestMessage ( void );
//    public string getRawResponseMessage ( void );
//    public HttpMessage getRequestMessage ( void );
//    public string getResponseBody ( void );
//    public int getResponseCode ( void );
//    public array getResponseCookies ([ int $flags = 0 [, array $allowed_extras ]] );
//    public array getResponseData ( void );
//    public mixed getResponseHeader ([ string $name ] );
//    public mixed getResponseInfo ([ string $name ] );
//    public HttpMessage getResponseMessage ( void );
//    public string getResponseStatus ( void );
//    public array getSslOptions ( void );
//    public string getUrl ( void );
//    public bool resetCookies ([ bool $session_only = false ] );
//    public HttpMessage send ( void );
//    bool setBody ([ string $request_body_data ] );
//    public bool setContentType ( string $content_type );
//    public bool setCookies ([ array $cookies ] );
//    public bool setHeaders ([ array $headers ] );
//    public bool setMethod ( int $request_method );
//    public bool setOptions ([ array $options ] );
//    public bool setPostFields ( array $post_data );
//    public bool setPostFiles ( array $post_files );
//    public bool setPutData ([ string $put_data ] );
//    public bool setPutFile ([ string $file = "" ] );
//    public bool setQueryData ( mixed $query_data );
//    public bool setRawPostData ([ string $raw_post_data ] );
//    public bool setSslOptions ([ array $options ] );
//    public bool setUrl ( string $url );
//}
//
//
//HttpMessage implements Iterator , Countable , Serializable
//{
//    public void addHeaders ( array $headers [, bool $append = false ] );
//    public __construct ([ string $message ] );
//    public HttpMessage detach ( void );
//    static public HttpMessage factory ([ string $raw_message [, string $class_name = "HttpMessage" ]] );
//    static public HttpMessage fromEnv ( int $message_type [, string $class_name = "HttpMessage" ] );
//    static public HttpMessage fromString ([ string $raw_message [, string $class_name = "HttpMessage" ]] );
//    public string getBody ( void );
//    public string getHeader ( string $header );
//    public array getHeaders ( void );
//    public string getHttpVersion ( void );
//    public HttpMessage getParentMessage ( void );
//    public string getRequestMethod ( void );
//    public string getRequestUrl ( void );
//    public int getResponseCode ( void );
//    public string getResponseStatus ( void );
//    public int getType ( void );
//    public string guessContentType ( string $magic_file [, int $magic_mode = MAGIC_MIME ] );
//    public void prepend ( HttpMessage $message [, bool $top = true ] );
//    public HttpMessage reverse ( void );
//    public bool send ( void );
//    public void setBody ( string $body );
//    public void setHeaders ( array $headers );
//    public bool setHttpVersion ( string $version );
//    public bool setRequestMethod ( string $method );
//    public bool setRequestUrl ( string $url );
//    public bool setResponseCode ( int $code );
//    public bool setResponseStatus ( string $status );
//    public void setType ( int $type );
//    public HttpRequest|HttpResponse toMessageTypeObject ( void );
//    public string toString ([ bool $include_parent = false ] );
//}

class HttpRuntimeException extends Exception
{
}

class HttpRequestException extends Exception
{
}

class HttpMalformedHeaderException extends Exception
{
}

class HttpEncodingException extends Exception
{
}

function http_request_method_name($method)
{
    return HTTPRequest::$methods[$method];
}

define('HTTP_METH_GET', HTTPRequest::METH_GET);
define('HTTP_METH_POST', HTTPRequest::METH_POST);

define('HTTP_AUTH_BASIC' , HTTPRequest::AUTH_BASIC);

define('HTTP_SSL_VERSION_ANY', HTTPRequest::SSL_VERSION_ANY);